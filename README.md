# Baguette Factory

[![Actions Status](https://gitlab.com/baguetteswap/baguetteswap-core/workflows/CI/badge.svg)](https://gitlab.com/baguetteswap/baguetteswap-core/actions)

In-depth documentation on BaguetteSwap is available at [docs.baguetteswap.exchange](https://docs.baguetteswap.exchange/).

# Local Development

The following assumes the use of `node@>=10`.

## Install Dependencies

`yarn`

## Compile Contracts

`yarn compile`

## Run Tests

`yarn test`
